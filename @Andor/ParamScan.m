function   [data_all, self] = ParamScan(self,motor, varargin )
    %% Author: Michal Odstrcil, mo1e13@soton.ac.uk, University Southampton

    par = inputParser;
    par.KeepUnmatched = true;
    par.addParameter('ShowOnlyROI',  false, @islogical)
    par.addParameter('logscale',  false, @islogical)
    par.addParameter('Precision', 1e-4, @isnumeric)
    par.addParameter('RemoveBackground', false, @islogical)
    par.addParameter('ReadOutTimeInd', self.ReadOutTimeInd, @isint)
    par.addParameter('Ndim_result', 1, @isint)  % Ndim of the grabed image 
    par.parse(varargin{:})
    r = par.Results;


    if r.Ndim_result == 1
%         N = 100;
        values = zeros(0,1);
        while true 
%             keyboard
            try
                [motor, pos, axis_names ] = motor.move_next_pos();
            catch
                disp('move pos failed, maybe last move')
                break
            end
            [data, self] = self.GetFVB(varargin{:},  'auto_save', false);
                
            if r.RemoveBackground
                 [background, self] = self.get_FVB_data(varargin{:}, 'ShutterMode', 2, 'auto_save', false); 
                 data = double(data) - double(background);
                 data(data<0) = 0;
            end
            data = medfilt1(double(data), 5);
            % data(data>quantile(data, 0.99)) = nan;
            values = [values; [pos, nanstd(data)]];

            fig = figure(3132);
            subplot(1,2,1)
            warning('off','all')
            set(gcf, 'Visible', 'on')
            try
                self.Show(values, 'curr_fig', fig, 'enhance_image', false);
                xlabel(axis_names{1})
                ylabel(axis_names{2})
                amax = argmax(values(:,3));
                title(sprintf('Position of maximum: %g, %g', values(amax,1), values(amax,2) ))
                
            end
            warning('on','all')

%             keyboard
            
            try
                subplot(1,2,2)
                plot(data)
                axis tight
                ylim([0, nanmax(data)])
                drawnow()
            end
            data_all = values;
        end
        values_tmp = values;
        if ~r.logscale
            values_tmp(:,3) = values_tmp(:,3) - median(values_tmp(:,3));
        end
        values_tmp(values_tmp(:,3)<0,3) = 0;
        self.Show(values_tmp, 'logscale', r.logscale);
       
        xlabel(axis_names{1})
        ylabel(axis_names{2})

        amax = argmax(values(:,3));
        t = (sprintf('Position of maximum: %g, %g', values(amax,1), values(amax,2) ));
        disp(t)
        title(t)

        
        path = datestr(now, 'yyyymmdd');
        if ~exist(path, 'file')
            mkdir(path);
        end
        name = ['/AndorScan_', datestr(now, 'HH-MM-SS')];
        print('-dpng', [path,  name, '.png'])
        motor_pos = motor.get_pos();
        save([path, name ], 'values', 'motor_pos')
        
        disp(['Saving as ', path, name])
        
        self.CloseShutter
        self.ShutdownCamera
        
    elseif  r.Ndim_result == 2
        
        win = 60;

        Npix = 1024;
        data_all = zeros(Npix, Npix, 0);
        pos_all = zeros(0,1);

        while true 
            try
               [motor, pos ] = motor.move_next_pos();
            catch
                break
            end
            pos_all = [pos_all, pos];
            try; self.CloseShutter; end  %% close shutter 
            % collect data 
            data = single(self.GetData(varargin{:}));
%             self.CloseShutter  %% close shutter 
            data_filt = medfilt2(data, [5,5], 'symmetric');
%             subplot(1,2,1)
            if r.ShowLogScale
                 imagesc(log(data))
            else
                 imagesc(data)
            end
            axis image 
            if r.ShowOnlyROI
                 [y,x] = find(data_filt == max(data_filt(:)), 1, 'first');
                 axis([max(1,x-win),min(Npix,x+win), max(1,y-win),min(Npix,y+win)]);
            end
            colorbar
            title(sprintf('Pos %.2gmm ' , pos))
%             subplot(1,2,1)
%             plot()
            drawnow()
            pause(0.01)
            data_all = cat(3, data_all, data);
        end
        N = size(data,3);
        
         pause(1) % clean ccd ? 
         [~,~, Noise] = self.GetBackground(varargin{:});

         for i = 1:N
              data_all(:,:,i)  = data_all(:,:,i)  - Noise;
         end


        [Npix, ~, N] = size(data_all);
        data_filt = medfilt2( data_all(:,:,end/2), [5,5]);
        tmp = max(data_filt, [],3);
        [y,x] = find(tmp == max(tmp(:)), 1, 'first');


        % find threshold 
        T = median(data_all(data_all > 0)) + 6*1.66*mad(data_all(data_all > 0), 1);

        data_small = zeros(win*2+1, win*2+1, N);

        for i = 1:N
            data = data_all(:,:,i);
            mask = data > T;
            mask = crop_outliers(mask);
            mask = imdilate(mask,  strel('disk',1)); % remove solitare pixels   
            data(~mask) = 0;
            imagesc(log( 5+ abs( data)))
            axis([max(1,x-win),min(Npix,x+win), max(1,y-win),min(Npix,y+win)]);
            colorbar
            title(['pos ', num2str(grid(i)), ' num ', num2str(i) ])
            drawnow
        %     print(['scan_', num2str(i),'.png'], '-dpng', '-r120')
            pause(0.01)
            data_small(:,:,i) = data(max(1,y-win):min(Npix,y+win), max(1,x-win):min(Npix,x+win));
        end


    end
        
        
end
            
