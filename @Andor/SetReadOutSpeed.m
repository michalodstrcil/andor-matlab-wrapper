function self = setReadOutSpeed(self,ReadOutTimeInd)
    %% Author: Michal Odstrcil, mo1e13@soton.ac.uk, University Southampton

    ElectronMultiply = 0 ; % no effect ... 
    [~,noHspeeds]=GetNumberHSSpeeds(0, ElectronMultiply);
    Hspeed = min(ReadOutTimeInd, max(0, noHspeeds-1));
    [~,noVspeeds]=GetNumberVSSpeeds;
    Vspeed = min(ReadOutTimeInd, max(0, noVspeeds-1));
    if self.verbose
         fprintf('Hspeed %i Vspeed %i \n', Hspeed, Vspeed)
    end
    [~]=SetHSSpeed(ElectronMultiply, Hspeed);
    [~]=SetVSSpeed(Vspeed);   %%% 0 is the fasters !!!
%         
    ReadOutTimeInd = min(max(noHspeeds,noVspeeds)-1, ReadOutTimeInd);
%     ReadOutTimeInd

    if strcmp( self.getModel, 'DO934P_BN')
        sat_levels=[3.5e4, 4e4, 5.6e4];
        self.sat_level = sat_levels(1+log2(self.getGain)) ;
    elseif strcmp( self.getModel, 'DV434')
        switch ReadOutTimeInd
            case 0, self.sat_level = 1.2e4;   % level when the camera is oversatureated 
            case 1, self.sat_level = 1.3e4 ;
            case 2,  self.sat_level = 1.7e4 ;
            case 3, self.sat_level = 4e4 ;
        end
    elseif isempty(self.getModel)
        warning('Unknown camera type, using DV434')
        switch ReadOutTimeInd
            case 0, self.sat_level = 1.2e4;   % level when the camera is oversatureated 
            case 1, self.sat_level = 1.3e4 ;
            case 2,  self.sat_level = 1.7e4 ;
            case 3, self.sat_level = 4e4 ;
        end
        
    else
        warning('Unknown camera type')
        keyboard
    end
    self.ReadOutTimeInd = ReadOutTimeInd;

end
