function [counts_per_photon, Qe] = getCounts2Photons(self, varargin)
    %% Author: Michal Odstrcil, mo1e13@soton.ac.uk, University Southampton

    par = inputParser();
    par.KeepUnmatched = true;
    par.addParameter('ReadOutTimeInd', self.ReadOutTimeInd, @isint)
    par.addParameter('Gain', self.getGain, @isint)
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%
    par.addParameter('Wavelenght', self.Wavelenght , @isnumeric)
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%

    par.parse(varargin{:})
    r = par.Results;
    
 
%     keyboard
    
    Ep = 1230 / r.Wavelenght / 1e9;  %  nm => eV
%  keyboard
 
    if strcmp(self.getModel, 'DO934P_BN')
        sensitivity = [6.5, 5.3, 5.2, 5.1] / r.Gain;
    elseif  strcmp(self.getModel, 'DV434')
        sensitivity = [2, 2, 1.4, 0.7];
    elseif isempty(self.getModel)
       warning('Unsure type of camera, using DX440')
        sensitivity = [2, 2, 1.4, 0.7];
        keyboard
    else
        error('Unknown camera')
    end
    
    %% from andor manual, quantum effeciency
    x = [20, 90];
    y = [0.7, 0.9];
    p = polyfit(log10(x),y,1);
    if Ep < x(1) || Ep > x(2)
        error('Out of allowed energy range')
    end
    Qe = polyval(p, log10(Ep));

    %% from andor manual, photon to photoelectons 
    x = [ 2, 5];
    y = [1.4314, 3.3];
    p = polyfit(x,y,1);
    
    n_el = 10^polyval(p, log10(Ep));
   
    
    counts_per_photon = n_el / sensitivity(max(1,r.ReadOutTimeInd)) ;
 
end
