function self = Init(self)
    %% Author: Michal Odstrcil, mo1e13@soton.ac.uk, University Southampton

    installpath = fullfile(matlabroot,'toolbox','Andor','Camera Files');
    current_path = pwd;
%     keyboard
    if strcmp(getComputerName, 'boxerbill')
        apath = path;
    else 
        apath = '';
    end

    cd(installpath);
%     path = '';
    
    try
          [~,XPixels,~]=GetDetector;    
           assert(XPixels*XPixels ~= 0)
    catch
%         self.ShutdownCamera();
%         pause(1)
        disp('Start Andor Camera Control');
        returnCode = AndorInitialize(apath);
%         returnCode = AndorInitialize('');
        if returnCode == 20002
            disp('Camera Initialized');
        elseif returnCode == 20003
            error('Unable to communicate with drivers, restart matlab  ????')
        elseif returnCode == 20013
            error('Unable to communicate with Andor card')
       elseif returnCode == 20992
            error('Drivers are used, close other programs ')
        else
            error(['Unknown error: ',num2str(returnCode),', try reboot matlab'])
        end
    end
    cd(current_path)

%     if nargin > 0
%         self = self.InitImage();
%     end
% 
%     disp('Setup ready')

  
end
