function self = InitImage(self )
    %% Author: Michal Odstrcil, mo1e13@soton.ac.uk, University Southampton
    if self.verbose
        disp(['self.Binning ', num2str(self.Binning)])
        disp(['self.ReadOutTimeInd ', num2str(self.ReadOutTimeInd)])
    end

    ShutterType = 1;% TTL high
    ClosingTime = 0;% ms
    OpeningTime = 0;
    TriggerMode = 0;% internal
    ReadMode = 4;% Image  

%% technical parameters
    [~]=SetReadMode(ReadMode);
    [~]=SetTriggerMode(TriggerMode);

    ClosingTime = 0;% ms
    OpeningTime = 0;
    TriggerMode = 0;% internal
    AccCycleTime = 0;
    ReadMode = 4;% Image
    AcquisitionMode = 1 ; %   Set acquisition mode; 1 for Single scan 


    %%%%%%%%%%%%% SET READOUT SPEED %%%%%%%%%%%%%%
    self.setReadOutSpeed(self.ReadOutTimeInd);
    
    
%% final image settings 
%%%%%%%%%%%%%%%%%%%%%%%%
% keyboard

    ROI = self.getROI;
    Hstart = 1+ROI(1)*self.Binning;
    Hend = (ROI(1)+ROI(3))*self.Binning;
    Vstart = 1+ROI(2)*self.Binning;
    Vend = ( ROI(2)+ROI(4))*self.Binning;

    [~]=SetImage(self.Binning,self.Binning,Hstart,Hend,Vstart,Vend);
%%%%%%%%%%%%%%%%%%%%%%%%%

    
%     [~]=SetOutputAmplifier(OutputAmplifier);  %  amplify the signal to suppress readout noise 
%     [~]=SetHighCapacity(HighCapacityMode);  %  switch between high capacitz and high sensitivity 
    self.ReadMode = ReadMode;
    self.FVB = false;


end