function [Flux] = GetFlux(self, varargin)
    %% Author: Michal Odstrcil, mo1e13@soton.ac.uk, University Southampton

    par = inputParser();
    par.KeepUnmatched = true;
    par.addParameter('Exposure',  1 , @isnumeric)
    par.addParameter('ReadOutTimeInd', self.ReadOutTimeInd, @isint)
    par.addParameter('Binning', 1, @isint)
    par.addParameter('Cropping', 1, @isint)
    par.addParameter('Gain', self.Gain, @isint)
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%
    par.addParameter('Wavelenght', self.Wavelenght , @isnumeric)
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%

    par.parse(varargin{:})
    r = par.Results;
    
   
    [counts_per_photon, Qe] = self.getCounts2Photons(varargin{:});
 
    keyboard
    
 
%  
%     Ep = 1230 / r.Wavelenght / 1e9;  %  nm => eV
%  
%     %% if strcmp(self.getModel, 'iKon')
% 
%     sensitivity = [6.5, 5.3, 5.2, 5.1] / r.Gain;
%     %% if  strcmp(self.getModel, 'DX400')
%     sensitivity = [2, 2, 1.4, 0.7];
% 
%     %% from andor manual, quantum effeciency
%     x = [20, 90];
%     y = [0.7, 0.9];
%     p = polyfit(log10(x),y,1);
%     if Ep < x(1) || Ep > x(2)
%         error('Out of allowed energy range')
%     end
%     Qe = polyval(p, log10(Ep));
% 
%     %% from andor manual, photon to photoelectons 
%     x = [ 2, 5];
%     y = [1.4314, 3.3];
%     p = polyfit(x,y,1);
%     
%     n_el = 10^polyval(p, log10(Ep));
%    
%     
%     counts_per_photon = n_el / sensitivity(1+r.ReadOutTimeInd) ;
%  

[DarkNoise,ReadOutNoise, DeadPixelMap, ShotNoise] = ...
 self.GetBackground(varargin{:});

 Data = self.GetData(varargin{:},  'ShutterMode', 0);
 
 if any(Data(:)) > self.sat_level
     warning('Data are oversaturated, use shorter exposure')
 end
 
 Data = Data - ReadOutNoise - r.Exposure*DarkNoise;
 Data = replace_nans(Data, DeadPixelMap);
 
 Data = posit(Data - ShotNoise - sqrt(r.Exposure*DarkNoise) );  %  remove noise levels 

 Flux = round(Data / counts_per_photon / Qe ) / r.Exposure ; %  calculate number of photons per second !! 
 
fprintf('Max photons per pixel per s %g +- %g', max(Flux(:)), sqrt( max(Flux(:))))
fprintf('Mean photons per pixel  per s %g +- %g', mean(Flux(:)), mean(sqrt( Flux(:))))
fprintf('Total photons  per s %g +- %g', sum(Flux(:)), sqrt( sum(Flux(:))))

 
 
end
