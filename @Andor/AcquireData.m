function data = AcquireData(self)
    %% Author: Michal Odstrcil, mo1e13@soton.ac.uk, University Southampton
    self.Abort();  % stop previous acquisitions 
    %% just readout data for the current setting 
    [~]=StartAcquisition;
    [~,gstatus]=AndorGetStatus;
    if self.verbose ;  disp('Acquire data'); end 
    t = tic;
    i = 1;
    while(gstatus ~= 20073)%DRV_IDLE
        pause(0.01);
        [~,gstatus]=AndorGetStatus;
        if mod(i, 100) == 0 
            if toc(t) < self.Exposure
                fprintf('.')
            else
                fprintf(':')
            end
        end 
        if  mod(i, 5000) == 0 
                fprintf('\n')
        end
        i = i+1;
    end
    if  i > 100
        fprintf('\n')
    end
    Npix = self.getNpix;
    if self.FVB
        [~,data]=GetAcquiredData(Npix(1) ); 
    else
        [~,data]=GetAcquiredData(prod(Npix));
    end

end
