function self = setNumAcc(self,NumAcc)
    %% Author: Michal Odstrcil, mo1e13@soton.ac.uk, University Southampton
    AccCycleTime = 0;

    if NumAcc <= 1
        AcquisitionMode = 1 ; %   Set acquisition mode; 1 for Single scan 
    else
        AcquisitionMode = 2 ; %   Set acquisition mode; 2 for Accumulate Scan
        SetFilterMode(2);  % set up cosmic rays filter
    end
    [~]=SetAcquisitionMode(AcquisitionMode);
    [~]=SetAccumulationCycleTime(AccCycleTime);
    [~]=SetNumberAccumulations(NumAcc);
    self.NumAcc = NumAcc;
end
