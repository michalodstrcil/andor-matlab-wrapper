classdef Andor  < handle
%% Author: Michal Odstrcil, mo1e13@soton.ac.uk, University Southampton


%% METHODS:

% disp()                        print info 


    properties (GetAccess = 'public', SetAccess = 'public')
        MaxTemperature;
        sat_level; 
        verbose;
    end
    
    properties (GetAccess = 'private', SetAccess = 'private')
%         XPixels;
%         YPixels;
        NumAcc;
        ReadMode;  %  0-3 integers, readout speed of the camera 
        ShutterMode
        exposure_range;
        ROI;
        ReadOutTimeInd;
        Temp;
        Binning; % 
        Cropping;
        XPS; 
        Smaract;
        FVB; % full vertical binning
        Gain;
        Exposure;
        Wavelenght;
    end
   
    methods ( Access = 'public' )
        function disp(self)
            fprintf('Current Temp %g C \n', self.getTemp)
            fprintf('Requested Temp %g C \n', self.Temp)
            fprintf('Binning %g \n', self.getBinning)
            fprintf('Cropping  %g \n', self.Cropping)
            fprintf('ReadOutInd  %g \n', self.getReadOutSpeed)
            fprintf('Exposure %gs \n', self.getExposure)
            fprintf('Gain %g \n', self.getGain)
            fprintf('Sensitivity (photons to counts)  %3.2g \n', self.getCounts2Photons)
            fprintf('Wavelenght  %g nm \n', self.Wavelenght*1e9)
        end
        
        function self = Andor(varargin)
            self.verbose = false;
            % Constructor of XPS 
            par = inputParser;
            par.addParameter('Smaract',[]) 
            par.addParameter('XPS',[]) 
            par.parse(varargin{:})
            r = par.Results;
            self.Temp = nan;
            self.Binning = 1;
            self.Cropping = 1;
            self.Smaract = r.Smaract;
            self.XPS = r.XPS;
            self.Exposure = 1;
            self.ShutterMode = 0;
            self.ReadOutTimeInd = 1;
            self.NumAcc = 1;
            self.exposure_range = [0.008, 1e3];
            self.MaxTemperature=28;
            self.sat_level=2e4;
            self.ROI = [0,0,1024,1024];
            self.FVB = false;
            self.Gain = 0;
            self.Wavelenght = 17.3e-9;% roughly value for EUV light and longest readout
           
            fileparts(mfilename('fullpath'))


            global base_path 
            base_path = [fileparts(mfilename('fullpath')), '\..\'];
%             base_path = 'C:\Users\michal\Dropbox\Scripts\';
            addpath(fullfile(matlabroot,'toolbox','Andor'))
            addpath(base_path)
            addpath(fullfile(base_path, 'LabJack'))
            addpath(fullfile(base_path, 'Matlab_XPS'))
            addpath(fullfile(base_path, 'utils'))

            self = self.Init();
            self.AutoShutter();
            
        end
        
        function self = PrepareCamera(self)
               [status,XPixels,YPixels]=GetDetector();          %   Get the CCD size
               if status == 20075
%                     warning('DRV_NOT_INITIALIZED')
                    self = self.Init();
                    [~,XPixels,YPixels]=GetDetector();          %   Get the CCD size
                end
                if XPixels == 0
                    warning('Trying to reinitialize camera')
                    self = self.Init();
                end
        end
        
        function self=OpenShutter(self)
            disp('Opening shutter')
            self = setShutter(self, 1);
        end

        function self=CloseShutter(self)
            disp('Closing shutter')
            self= setShutter(self, 2);
        end
        
        function self = setShutter(self, id)
            self=self.PrepareCamera();
            self.ShutterMode = id;
            [~]=SetShutter(1,id, 1, 1);
            if self.verbose
                switch id
                    case 1,  disp('Shutter closed')
                    case 2,  disp('Shutter opened')
                    case 0,  disp('Shutter auto')
                end
            end
        end
        function id = getShutter(self)
            id = self.ShutterMode;
        end
        function self = setBinning(self, binning)
            [~,MaxBinning]=GetMaximumBinning(self.ReadOutTimeInd, 0);
            if MaxBinning < binning
                warning('Given binning is larger than maximal allowed')
            end
            binning = max(1,min(binning,MaxBinning ));
            self.Binning = binning;
        end
        function binning = getBinning(self)
            binning = self.Binning;
        end
        function model = get_model(self)
            [~,model]=GetHeadModel('');
        end
        function self = setGain(self, gain)
            gain = floor(log2(max(1,gain)));
            [~, n_gains] = GetNumberPreAmpGains();
            
            if n_gains == 0
                error('Preamplifier gain setting is not supported')
            end
            if gain > n_gains-1
                warning('too high gain')
                gain = n_gains-1;
            end
            [~, availible] = IsPreAmpGainAvailable(0,0,0,gain);
            if ~availible
                error('Selected gain is not availible')
            end
            [~]=SetPreAmpGain(gain);
            self.Gain = gain;
        end
        function gain = getGain(self)
            [~, gain]=GetPreAmpGain(self.Gain);
        end
        
        % different methods for displaying the current data 
        data = Show(self, varargin);  % params: data,  background - zeros(Npix), logscale - false
        ShowFVBContinuous(self, varargin); %  params:  Exposure: background - zeros(Npix), logscale - false
        ShowContinuous(self, varargin); % params:  Exposure
        

        %  read out background noise 
        %  params: fast - logical - get only fast guess (optional)
        %  params: Exposure - double - time for background noise
        %  integrating (optional)
        %  params: Temp - double -  desired temperature of the camera (NaN
        %  is default
        %  params: ReadOutTimeInd - inxed of readout mode, lower => faster 
        [DarkNoise,ReadOutNoise, DeadMap, ShotNoise] = ...
            GetBackground(self,varargin);
        
        

        % load standard 2D image using predefined settings 
        %  params: fast_mode - logical - get only fast guess (optional)
        %  params: Exposure - double - time for background noise
        %  integrating (optional)
        %  params: Temp - double -  desired temperature of the camera (NaN
        %  is default
        %  params: ReadOutTimeInd - inxed of readout mode, lower => faster NumAcc
        %  params: ShutterMode - set Shutter mode (default is auto)
        %  params: auto_save - automaticaly save the loaded data to disk 
        [data, oversat, object, self] = GetData(self, varargin );
        % Get data using full vertical binning -> very fast
        [data, self] = GetFVB(self,varargin);
        
        % Get data with high dynamic range, maximal exposure time =
        % 'Exposure' and minimal is scanned in order to avoid
        % oversaturation
        [HDR_data,HDR_noise, mask, input, object, self] = GetHDR(self,varargin);
        % do scanning, takes scanning motor object (xps, smaract) and
        % readout picture in each scan position
        [data, self] = ParamScan(self,scan_motor, varargin );
        % calculate photon flux incidenring the CCD
        [Flux] = GetFlux(self,varargin);
        % get constants to calculate flux 
        [counts_per_photon, Qe] = getCounts2Photons(self, varargin);

        
        % smart saving function, saves data + image of the data 
        object = Save(self, data, name, varargin );
        function Clear(self, N )
            if nargin < 2
                N = 5;
            end
            disp('========  cleaning signal  ===========')
           [~] = GetData(self,'verbose', false,'Binning',8, 'Exposure', 0.01,'ShutterMode', 2,...
               'ReadOutTimeInd',1, 'NumAcc', N, 'auto_save', false, 'fast_mode', true); 
        end
        function self = setExposure(self,exposure)
            exposure = min(max(exposure,  self.exposure_range(1)),  self.exposure_range(2));
            [~]=SetExposureTime(double(exposure));
            self.Exposure = exposure; 
        end
        function [exposure, exposure_range] = getExposure(self)
           exposure =  self.Exposure;
           exposure_range = self.exposure_range;  % ms 
        end
        
        function Npix=getNpix(self)
            ROI = self.getROI;
            Npix = ROI(3:4);
        end
        function Npix = getAllPix(self)
             [~,XPixels,YPixels]=GetDetector(); 
             Npix = [XPixels,YPixels];
        end
        
             
        function currentTemp = getTemp(self)
            [~,currentTemp]=GetTemperature;
        end
        % cool camera to value Temp, if NaN it just skip cooling 
        setTemp(self,Temp, fast);
   

        function ROI = getROI(self)
            ROI = self.ROI;
        end
        
        function ShutdownCamera(self)
            disp('Andor shutdown')
            [~]=AndorShutDown;
        end
        function delete(self)
            self.ShutdownCamera();
        end
        self = setReadOutSpeed(self,ReadOutTimeInd);
        function id  = getReadOutSpeed(self)
            id = self.ReadOutTimeInd;
        end
        function self  = Restart(self)
            self.ShutdownCamera;
            self = Andor();
            self.setTemp(self.Temp, true);
        end
        function self  = Abort(self)  
            [~]=AbortAcquisition();
            [~]=CancelWait();
        end
        function ccd_model_name=getModel(self)
           [~,ccd_model_name] = GetHeadModel;
        end
                
        % just readout data for the current setting 
        data = AcquireData(self)
        
        
    end
    
    
    
    
    
    methods ( Access = 'private' )
        %%%%%%%%%%%%%%%% PRIVATE %%%%%%%%%%%%%%%%%%%%%%%%%%
        function self=AutoShutter(self)
            disp('Auto shutter')
            self = setShutter(self, 0);
        end

        % load Andor drivers 
        self = Init(self);
        
        % Init settings for full vertical binning
        self = InitFVB(self);
        % Init settings for normal readout 
        self = InitImage(self);

        % set number of accummulations 
        self = setNumAcc(self,NumAcc);
        % set the readout speed (ReadOutTimeInd in [0-3])

        function setROI(self, xmin, ymin, W, H)
%             [~,XPixels,YPixels]=GetDetector();          %   Get the CCD size
            Npix =  self.getAllPix() / self.getBinning;
            xmin = min(max(0, xmin), Npix(1));
            ymin = min(max(0, ymin), Npix(2));
            W = min(W, Npix(1) - xmin );
            H = min(H, Npix(2) - ymin);
            self.ROI = [xmin, ymin, W, H];
        end

    end
end
