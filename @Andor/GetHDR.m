function [HDR_data_out,HDR_noise_out, over_sat_mask, input, object, self] ...
    = GetHDR(self, varargin )
    %% Author: Michal Odstrcil, mo1e13@soton.ac.uk, University Southampton

    addpath('utils')
    par = inputParser();
    par.addParameter('Show',  false , @islogical)
    par.addParameter('auto_save',  true , @islogical)
    par.addParameter('Exposure', 5 , @isnumeric)
    par.addParameter('Binning', 1 , @isint)
    par.addParameter('Cropping', 1 , @isint)
    par.addParameter('Gain', 1 , @isint)
    par.addParameter('crop_center', [] , @isnumeric)
    par.addParameter('fast_mode', true , @islogical)
    par.addParameter('NumAcc', 1 , @isint)
    par.addParameter('Background', [] )
%     par.addParameter('CorrectExposure',  true , @islogical)
    par.addParameter('ReadOutTimeInd',  self.ReadOutTimeInd , @isint)
    par.addParameter('Temp',  NaN , @isnumeric)
    par.addParameter('Comment',  '', @isstr )

    par.parse(varargin{:})
    r = par.Results;

    %%%%%%%%%%%%%%%%%%%%%%% COOLING %%%%%%%%%%%%%%%%%%%%
    self.setTemp(r.Temp, false);
    %%%%%%%%%%%%%%%%%%%%%%% COOLING %%%%%%%%%%%%%%%%%%%%
    
    
    self.setROI(0,0,1024, 1024)
    NumAcc =  r.NumAcc;

    
%     varargin = [varargin , 'fast_mode', true];
    
    if isempty(r.Background)
        disp('=============== Read background ================= ')
        if ~r.fast_mode
            exposure_background = max(5, r.Exposure);
        else
            exposure_background = max(5, sqrt(r.Exposure));
        end
        [DarkNoise,ReadOutNoise, DeadPixelMap, ShotNoise] = ...
                self.GetBackground(varargin{:},'NumAcc', NumAcc, ...
                'Exposure',exposure_background , 'fast_mode', true);
        r.Background = {DarkNoise,ReadOutNoise, DeadPixelMap, ShotNoise};
    else
        ReadOutNoise = r.Background{2};
        DarkNoise = r.Background{1};
        DeadPixelMap = r.Background{3} > 0;
        ShotNoise = r.Background{4};
        self.Clear();
    end
    
%     keyboard 
    
    self = self.PrepareCamera();
    self.setROI(0,0,1024, 1024)

    self.setGain(r.Gain);

    self.setBinning(r.Binning);
    self = self.InitImage();

    
    Npix = self.getAllPix()/ r.Binning;
    
    if ~isempty(r.crop_center) && r.Cropping == 1
        warning('Cropping is 1')
        r.crop_center = [];
    end
    
    if ~isempty(r.crop_center) && any(r.crop_center > Npix ) && r.Cropping > 1
        error('Cropping center out of image')
    end
        
    mask_sat_all = zeros(Npix);

    fprintf('Saturation level %i \n', self.sat_level )
    
    
%     keyboard
    [~, ROI_final]=crop_image(true(Npix), r.Cropping, r.crop_center);

    
    %% minimal exposure time allowed for each read out speed 
    min_exp = [0.008, 0.1, 1, 5,20];
    min_exp  = min_exp(1+r.ReadOutTimeInd);
     
        
    r.Exposure  = max(r.Exposure, min_exp);
    
    
    t0 = tic;
    ii = 1;
    disp('=============== Read data ================= ')
    while true
        try
           
            if r.fast_mode
                Exposure = r.Exposure*0.1^(ii-1);
                [~, ROI] = get_ROI(mask_sat_all >= max(0, ii-1), 0);
            else
                Exposure = r.Exposure*0.3^(ii-1);
                [~, ROI] = get_ROI(mask_sat_all >= max(0, ii-2), 0);
                Exposure = max(Exposure, min_exp);
            end
            if ii == 1
%                 keyboard
                [~, ROI]=crop_image(true(Npix), r.Cropping, r.crop_center());
            end
                
        catch
            keyboard
        end

        mask =  false(Npix);  % oversaturated values 
        wrong =  false(Npix); % some other camera issues 
              
        data = nan(Npix(1),Npix(2), NumAcc );
        
        if ii > 1
            extra_px = max(10,ceil(20/r.Binning));
        else
            extra_px = 0;
        end
        for l = 1:2 %% find the oversatureated area and crop it 
            ROI{l} = [ROI{l}(1)-extra_px:ROI{l}(1)-1, ROI{l}, ROI{l}(end)+1:ROI{l}(end)+extra_px];
            ROI{l} = ROI{l}( ROI{l} >= ROI_final{l}(1) & ROI{l} <= ROI_final{l}(end) );
        end

%         keyboard
        params = {'Show',false,'Exposure', Exposure, 'NumAcc', 1, 'Binning', r.Binning, 'ROI',ROI, 'fast_mode', true, 'ReadOutTimeInd', r.ReadOutTimeInd};
        for acc = 1:NumAcc
            for kk = 1:3
                try
                    [data(ROI{:}, acc), mask(ROI{:},acc), ~, self] = ...
                        self.GetData( params{:} );
                    mask_sat_all(mask) = ii;
                    wrong(ROI{1}(1),ROI{2}) = true; % the first line of the image is wrong 
                    break
                catch
                    if kk == 3
                        warning('Rebood didnt help')
                        keyboard
                    end
                    warning('Image acquisition failed, rebooting  camera ... ')
                    self.Restart();
                    pause(1)
                end
            end
        end
        data = single(data);
        oversat_px(ii) =  mean(mean(mask(2:end-1, 2:end-1)));
        
        % run median filter to clear the data in case of more accumulations
        % 
        
%         keyboard
        
        
%         N = size(data, 3);
%         if N > 4
%             med = sort(data,3);
%             tail = 0.1;
%             med = med(:,:,ceil(N*tail):floor(N*(1-tail)));
%         else
        med = data;
%         end
        data_clear = mean(med, 3);
        noise = zeros(size(data), 'single'); % std(med,[],3);

        % take real exposure that was used to read out this image  
        Exposure = self.getExposure();

%         keyboard
        
        %% crop the final image to the cropped size 
        data = data(ROI_final{:},:);
        data_clear = data_clear(ROI_final{:},:);
        noise = noise(ROI_final{:});
        mask = mask(ROI_final{:});
        wrong = wrong(ROI_final{:});

        input{ii}.data = data;
        input{ii}.noise = noise;
        input{ii}.exposure =  Exposure;     
        input{ii}.mask =  mask;
        input{ii}.wrong =  wrong;

        input{ii}.data_corr = single(data_clear)  - ReadOutNoise - input{ii}.exposure*DarkNoise;
        mask_tmp = true(Npix);
        mask_tmp(ROI{:}) = false;
        mask_tmp = mask_tmp(ROI_final{:});
        input{ii}.data_corr(mask_tmp) = 0;
        

%         % medfi
%         keyboard
        
        if mean2(input{ii}.data_corr < 0) > 0.6
            warning('Background subtraction failed ? %i%% values are negative',  round(mean2(input{ii}.data_corr < 0) * 100) )
            keyboard
        end
       if quantile(input{ii}.data_corr(:) , 0.01) > 2 * (max(sqrt( input{ii}.exposure*DarkNoise(:))) + ...
               max(ShotNoise(:)))
            warning('Background subtraction failed ? Too low dark noise estimation (%g) ', quantile(input{ii}.data_corr(:) , 0.01) )
%             keyboard
        end
        data_tmp= input{ii}.data_corr;
        try
            data_tmp = replace_nans( data_tmp, wrong);
        end
try
        figure(213)
        subplot(1,2,1)
        load MyColormap
        imagesc_log(mean2(ShotNoise)+posit(data_tmp));
        colormap(HDR_cmap)
        title(['Max ', num2str(nanmax(data_tmp(:)))])
        axis off image
        subplot(1,2,2)
        hist(log2(abs(data_tmp(data_tmp>1))),1000)
        xlim([0,nanmax(log2(abs(data_tmp(:))))])
        drawnow
        
catch err
        warning('Image plotting failed: %s ',  err.message )
end


        fprintf('Over-exposed pixels %1.3g %%  exp time %g s \n ', ...
            100*oversat_px(ii),  Exposure )

        if (ii > 1 || r.fast_mode ) && oversat_px(ii) == 0 
            break
        end
%         [~, Exposure_range]= self.getExposure;
         if Exposure <= min_exp
            if  oversat_px(ii)
                 warning('Oversaturated image')
            end
            break
         end
         if Exposure < 0.1
             warning('Too short exposure, possible nonlinearities')
         end
        ii = ii +1;
       save inputs_tmp 
    end
    
    toc(t0)
    
% keyboard

%     load inputs_tmp
    
    
%     keyboard
    
%     



    
    N_img = length(input);

    
    W_fun = @(x)(1./(1+x.^2));
    
    Npix = size(input{1}.data_corr);
    try
        %% calculate HDR 
        data_all = zeros(Npix);
        exp_all = zeros(Npix);
        for ii = 1:N_img
            data_tmp = input{ii}.data_corr;
            data_tmp(isnan(data_tmp)) = 0;
            W = W_fun((data_tmp+(self.sat_level/2))/(self.sat_level/4)); % make smooth stitching 
            wrong{ii} = input{ii}.wrong | any(input{ii}.data > 0.9*self.sat_level,3); %% number of oversaturated must only grow !!
            wrong{ii} = imdilate(wrong{ii},  strel('disk',2));
            data_tmp(wrong{ii})  = 0;
            
            %% new  code that is trying to estimate the optimal correction of the exposure time 
            if ii > 1
                T =  mean2(4 *  (ShotNoise + sqrt(input{ii-1}.exposure* DarkNoise)) /  input{ii}.exposure) ;  % pesimistic estimation of noise
                d0 = medfilt2(input{ii-1}.data_corr / input{ii-1}.exposure, [3,3])  ;
                d1 = medfilt2(input{ii}.data_corr / input{ii}.exposure, [3,3]) ;
                compare_ROI = imerode(d0 > T &  d1 > T & ~wrong{ii} & ~wrong{ii-1}, strel('disk',2));
                %% !! autocalculate correction to get optimal stitching !!!  
                ratio = median(d1(compare_ROI) ./ d0(compare_ROI));
                ratio_err = mad(d1(compare_ROI) ./ d0(compare_ROI),1);
                fprintf('Exposure correction ratio: %3.2g +- %3.1g  (%4.2gs -> %4.2gs)\n', ratio, ratio_err, ...
                    input{ii}.exposure , input{ii}.exposure * ratio)
                input{ii}.exposure =  input{ii}.exposure * ratio;
            end
            data_tmp = data_tmp.*W;
            data_all = data_all + data_tmp;
            exp_all = exp_all + input{ii}.exposure / r.Exposure .*W .* ~wrong{ii};    
        end
        HDR_data = single(data_all ./ exp_all);
    catch err
        keyboard
    end
    
    HDR_data = replace_nans(HDR_data,  DeadPixelMap);
    
    
    over_sat_mask = exp_all == 0;

        
%     keyboard
    
    %% calculate errors 
    std_all = zeros(Npix);
    diff_all = zeros(Npix);
    dcount_all = zeros(Npix);
    scount_all = zeros(Npix);
    good_count_all = zeros(Npix);

    for ii = N_img:-1:1
        data_tmp = input{ii}.data_corr;
        noise_tmp = single(input{ii}.noise).^2 + ShotNoise.^2 + abs(input{ii}.exposure*DarkNoise) ....
            + abs(input{ii}.data_corr) *self.getCounts2Photons() ;  % add all noise sources that are measured 
        data_tmp(isnan(data_tmp)) = 0;
        W = W_fun((data_tmp - self.sat_level/2)/(self.sat_level/4));
        wrong = input{ii}.mask | input{ii}.wrong | data_tmp > 0.9*self.sat_level; %% number of oversaturated must only grow !!
         W(W<0)=0;
        diff_tmp = medfilt2( (data_tmp / (input{ii}.exposure/r.Exposure) - HDR_data).^2 ,[5,5], 'symmetric');
        if any(~wrong(:))
            Wmin= min(W(~wrong));
            W = W -Wmin;
        end
        diff_tmp(wrong )  = 0;
        if ~isscalar(noise_tmp)
          noise_tmp(wrong | isnan(noise_tmp) )  = 0;
        end
        std_all = std_all +noise_tmp;
        diff_all = diff_all + diff_tmp.*W;
        counts = input{ii}.exposure / r.Exposure *~wrong;
        dcount_all = dcount_all +counts  .* (W+Wmin);
        scount_all = scount_all +  counts;
        good_count_all = good_count_all + ~wrong;
    end
        
    HDR_noise = single(sqrt( (std_all ./ scount_all) + (diff_all./dcount_all)) );
    wrong = (good_count_all == 0) | imfill(HDR_noise < quantile(HDR_noise(:), 0.001), 'holes');
%     HDR_noise(wrong) =  max(HDR_noise(:));
    HDR_noise (HDR_noise < quantile(HDR_noise(:), 0.01)) = nan;  %  if the noise is too underestimated 
    HDR_noise  = replace_nans(HDR_noise,DeadPixelMap | wrong );
    HDR_noise = medfilt2(HDR_noise, [3,3], 'symmetric');

%     keyboard
    

    time = now;

    save_path =  datestr(time, 'yyyymmdd');

    if nargout ~= 0
        HDR_data_out = HDR_data;
        HDR_noise_out = HDR_noise;
    end
    
    
%     keyboard
    
    try
        if r.auto_save
            object = self.Save( HDR_data,  ...
                ['Andor_HDR_', datestr(now, 'HH-MM-SS'), '-', r.Comment], ...
                    'Noise',HDR_noise,'Mask', over_sat_mask,...
                    'inputs', input, 'Background',...
                    r.Background, 'Comment', r.Comment, ...
                    'base_path', 'C:\AndorData\',  'logscale', true);
        end
    catch err
        warning('Saving failed: %s ',  err.message )
        keyboard
    end
    
    try
    if r.Show || nargout == 0

%         keyboard
        
        figure(12321)
        MIN = max([0.1,(posit(min(HDR_data(:)))), (2*median(ShotNoise(:)))]); %
%         MIN = max(0.1, log10(quantile(posit(HDR_data(:)), 0.005))); % log10(median(HDR_noise(:))/10);
        MAX = max(MIN+1, max(HDR_data(:))); %(quantile(HDR_data(:), 0.9999));
        ax(1)=subplot(1,2,1);
        imagesc_log((MIN+abs(HDR_data)), [MIN,MAX])
        load MyColormap
        colormap(HDR_cmap(1:8:end, :))
        axis off image 
        title('Data')

%         keyboard
        
        MIN = max(0.1, quantile(posit(HDR_noise(:)), 0.01)); % log10(median(HDR_noise(:))/10);
        MAX = (max(MIN+1, quantile(HDR_noise(:), 0.9999)));
        
        ax(2)=subplot(1,2,2);
        imagesc_log((1+HDR_noise), [MIN,MAX])
        colormap(HDR_cmap(1:8:end, :))
        axis off image 
        title('Noise')
        linkaxes(ax, 'xy')
        drawnow
%     keyboard
    
        if r.auto_save
            if ~exist(save_path, 'dir')
                mkdir(save_path);
            end
            if ~exist([save_path,'/img'], 'dir')
                mkdir([save_path,'/img']);
            end

            print('-dpng',[save_path,'/img/','HDR_img ',datestr(time,'HH-MM-SS') ,'.png'])
        end
    end
    catch err
        warning('Image plotting failed: %s ',  err.message )
%         keyboard
    end
end
