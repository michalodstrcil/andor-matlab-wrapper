function [ROI, range,  coord] = get_ROI(mask, extent)
    %% Author: Michal Odstrcil, mo1e13@soton.ac.uk, University Southampton

    if all(mask(:) == 0)
        error('Empty mask')
    end
    
    if nargin == 1  
       extent = 0.25;
    end

%     keyboard
    [W,H] = size(mask);
    try
    coord = [find(nansum(mask,2), 1,'first'), find(nansum(mask,2), 1,'last'), find(nansum(mask,1), 1,'first'), find(nansum(mask,1), 1,'last')];
    coord(1) =  floor(coord(1)  - ceil(extent * (coord(2) - coord(1)))) ;
    coord(2) =  ceil(coord(2)  + ceil(extent * (coord(2) - coord(1)))) ;
    coord(3) =  floor(coord(3)  - ceil(extent * (coord(4) - coord(3)))) ;
    coord(4) =  ceil(coord(4)  + ceil(extent * (coord(4) - coord(3)))) ;
    catch
        keyboard
    end
    
    
    coord(coord < 1) = 1;
    if coord(2) > W ; coord(2) = W; end
    if coord(4) > H ; coord(4) = H; end

%     keyboard
    
    ROI = false(size(mask));
    range = {coord(1):coord(2), coord(3):coord(4)};
    ROI(range{1}, range{2}) =  true;
 
        
end