function ShowFVBContinuous(self, varargin )
    %% Author: Michal Odstrcil, mo1e13@soton.ac.uk, University Southampton

    par = inputParser;
    par.KeepUnmatched = true;
    par.addParameter('get_background',  false , @islogical )
    par.addParameter('logscale', false, @islogical)
    par.addParameter('fast_mode', true, @islogical)  % plot every third step
    par.addParameter('NumAcc', 1e4, @islogical)  % plot every third step

    par.parse(varargin{:})
    r = par.Results;
    
    
    N = 500;
    i = 0;
    Npix = self.getNpix;
    data_all = nan(Npix(1), N);
    figure
    t0 = tic;
    tvec = nan(N,1);
    pos = nan(N,1);
    data = nan(N,1);
    data_min = nan(N,1);
    data_max = nan(N,1);

    for iter = 1:r.NumAcc
        tic
        i = mod(i, N)+1;
        
        tvec(min(i+1, N)) = nan;

        fprintf('Accumulation %i \n ', i)
        [data_FVB, self] = ...
            self.GetFVB('auto_save', false,'ShutterMode', 1, varargin{:});
        if r.get_background
            [background, self] = ...
                  self.GetFVB('auto_save', false, 'ShutterMode', 2,  varargin{:});
            data_FVB = data_FVB  - background;
            data_all( data_FVB < 0 ,i) = 0; 
        end       
        

        data_tmp = data_FVB;
        data_tmp = data_tmp  - mean(data_tmp);
        data_tmp(data_tmp<0) = 0;
        pos(i) = sum(data_tmp .* (1:length(data_tmp))') / sum(data_tmp);
        tvec(i) = toc(t0);
        data(i) =  nanmean(data_FVB);
        data_min(i) =  quantile(data_FVB, 0.01);
        data_max(i) =  quantile(data_FVB, 0.99);

        data_all = circshift(data_all, [0,-1]);
        
        data_all(:,end) = medfilt1(double(data_FVB), 3);
       


        if r.fast_mode && mod(i, 5) ~= 0;toc; continue; end

        subplot(1,2,1)
        if r.logscale
             imagesc(log(data_all))
        else
             imagesc(data_all)
        end
        axis off 
%         colorbar
        subplot(2,2,2)
        plot(tvec, data_max, 'r-')
        hold on
        plot(tvec, data, 'k.-')
        plot(tvec, data_min, 'b-')
        hold off
        xlabel('Time [s]')
        title(['Std signal  ', num2str(nanstd(data_max))])

%         legend({'max', 'mean', 'min'}, 'Location','NorthWest'  )
%         vline(tvec(i))
        hline(data(i))
%         axes('XLim',[nanmin(tvec), nanmax(tvec)])
        try;xlim([nanmin(tvec), nanmax(tvec)]); end
        subplot(2,2,4)
        plot(tvec, pos, '.-')
%         vline(tvec(i))
        hline( pos(i))
        xlabel('Time [s]')
        title(['Std noise  ', num2str(nanstd(pos))])
        ylabel('Position [px]')
%         axes('XLim',[nanmin(tvec), nanmax(tvec)])

        try;xlim([nanmin(tvec), nanmax(tvec)]); end
%         drawnow()
        toc
    end
end
