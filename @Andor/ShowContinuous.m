function ShowContinuous(self, varargin)
    %% Author: Michal Odstrcil, mo1e13@soton.ac.uk, University Southampton
%  
    par = inputParser;
    par.KeepUnmatched = true;
    par.addParameter('get_background',  false , @islogical )
    par.addParameter('logscale', false, @islogical)
    par.parse(varargin{:})
    r = par.Results;


    N = 100;
    i = 0;
    mean_all = nan(N,1);
    min_all = nan(N,1);
    max_all = nan(N,1);

    t0 = tic;
    tvec = nan(N,1);
    figure
    while true 
        tic
        i = mod(i, N)+1;
        tvec(min(i+1, N)) = nan;
        fprintf('Accumulation %i \n ', i)
        
%         keyboard
        
        [data,~,~,self] = ...
            self.GetData('auto_save', false,'fast_mode',true, varargin{:});
        data = single(data);
        if r.get_background
            data = data - self.GetBackground();
            data(data < 0) = 0 ; 
        end
        mean_all(i) = nanmean(data(:));
        min_all(i) = quantile(data(:), 0.001);
        max_all(i) = quantile(data(:), 0.999);

        subplot(1,2,1)
        if r.logscale
            data(data < 0) = 0;
            data = log(data);
        end
%         keyboard
        self.Show(data, 'curr_fig', gcf, 'logscale', r.logscale);
        axis off image 
        subplot(1,2,2)
        tvec(i) = toc(t0);
        plot(tvec, max_all, 'r-')
        hold on
        plot(tvec, mean_all, 'k.-')
        plot(tvec, min_all, 'b-')
        hold off
        xlabel('Time [s]')
        legend({'max', 'mean', 'min'})
        vline(tvec(i))
        drawnow()
        toc
    end

end