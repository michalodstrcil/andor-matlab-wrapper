function self = InitFVB(self)
    %% Author: Michal Odstrcil, mo1e13@soton.ac.uk, University Southampton

    TriggerMode = 0;% internal
    ReadMode = 0;% full vertical binning 
    AcquisitionMode = 1 ; %   Set acquisition mode; 1 for Single scan 
    [~]=SetAcquisitionMode(AcquisitionMode);
    [~]=SetReadMode(ReadMode);
    [~]=SetTriggerMode(TriggerMode);

    disp('Setup ready')
    
    
    self.ReadMode = ReadMode;
    self.FVB = true;
    
end