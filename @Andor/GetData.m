function [data_out, oversat,  object, self] = GetData(self, varargin )
    %% Author: Michal Odstrcil, mo1e13@soton.ac.uk, University Southampton

    object =  [];
    par = inputParser();
    par.KeepUnmatched = true;
    par.addParameter('Exposure',  1 , @isnumeric)
    par.addParameter('NumAcc', 1, @isint)
    par.addParameter('ReadOutTimeInd', self.ReadOutTimeInd, @isint)
    par.addParameter('Temp', self.Temp, @isnumeric)
    par.addParameter('ShutterMode', 0, @isint)
    par.addParameter('Binning', 1, @isint)
    par.addParameter('Cropping', 1, @isint)
    par.addParameter('Gain', self.Gain, @isint)
%     par.addParameter('GetBackground', false, @islogical)  % TODO 
    par.addParameter('fast_mode', true, @islogical)
    par.addParameter('auto_save',  false, @islogical)
    par.addParameter('save_path', '', @isstr)
    par.addParameter('Show',  false , @islogical)
    par.addParameter('logscale',  false , @islogical)
    par.addParameter('ROI',  {} , @iscell)
    par.addParameter('verbose', self.verbose , @islogical )
    par.addParameter('crop_center', [] , @isnumeric)

%     par.addParameter('clear', true , @islogical )

    
%     
%     par.addParameter('Exposure',  [] , @isnumeric)
%     par.addParameter('NumAcc', 1, @isint)
%     par.addParameter('Binning', 1 , @isint)  % hardware 
%     par.addParameter('Cropping', 1 , @isint)  % hardware 
%     par.addParameter('Show',  false , @islogical)
%     par.addParameter('ShutterMode',  0 , @isint)
%     par.addParameter('ExtShutterMode',  0 , @isint)
%     par.addParameter('ROI',  {} , @iscell)

 
    
%     keyboard
    
    par.parse(varargin{:})
    r = par.Results;

   
    
    self.verbose = r.verbose;
    
    self = self.PrepareCamera();

    self.setROI(0,0,1024, 1024)
    
    %% init the camera image mode 

%     keyboard
    
    %%%%%%%%%%%%%%%%%%%%%%% COOLING %%%%%%%%%%%%%%%%%%%%
    self.setTemp(r.Temp);
    %%%%%%%%%%%%%%%%%%%%%%% COOLING %%%%%%%%%%%%%%%%%%%%
    
%     [~,currentTemp]=GetTemperature;
    if self.getTemp >= self.MaxTemperature
        self.ShutdownCamera();
%         currentTemp;
        error('Camera temperature is too high, shutting down camera ...  (adjust MaxTemperature limit)')
    end
    
%     keyboard
    self=self.setGain(r.Gain);
    self=self.setShutter(r.ShutterMode);
    self=self.setReadOutSpeed(r.ReadOutTimeInd);
%     
    self=self.setExposure(r.Exposure);
    if self.verbose
        disp(['Exposure set to ', num2str(self.getExposure())])
    end
    
%     keyboard
    
    self=self.setBinning(r.Binning);
    

    if self.getExposure < 0.03 && self.getShutter == 0 
        warning('Too short exposure, shutter may not be opened')
    end
    
    if ~isempty(r.ROI)
        X0= double([r.ROI{1}(1)-1,r.ROI{2}(1)-1]);
        dX = double([r.ROI{1}(end)-r.ROI{1}(1)+1,r.ROI{2}(end)-r.ROI{2}(1)+1]);
    else
%         X0 = double([1024, 1024]/r.Binning*(1-1/r.Cropping)/2);
%         dX = double([ 1024-2*X0(1), 1024-2*X0(2)]);

        Np_all = self.getAllPix();
        [~, ROI]=crop_image(true(Np_all), r.Cropping, r.crop_center);
%         keyboard
        X0 = [ROI{1}(1),ROI{2}(1)]-1;
        dX = [length(ROI{1}),length(ROI{2})];
    end
    
   
    self.setROI(X0(1), X0(2), dX(1), dX(2));
    
    
    
    %%% call when all setting are ready 
    self = self.InitImage();

    
    
%     keyboard
    Npix = self.getNpix();
    
%     keyboard
    
    data = zeros(Npix(1), Npix(2),r.NumAcc, 'uint16');
%     wrong = false(Npix(1), Npix(2),r.NumAcc);
    oversat_tot = false(Npix);
   
    [~,validExpTime,~,~]=GetAcquisitionTimings;
try
%     data = zeros(self.XPixels/self.Binning/self.Cropping, self.YPixels/self.Binning/self.Cropping, r.NumAcc, 'uint16');
    for i = 1:r.NumAcc
        if self.verbose
        fprintf('Accumulation %i / %i \n ', i, r.NumAcc)
        end
        if validExpTime > 10
            disp(['Starting exposure ', num2str(validExpTime), 's ',  datestr(now, 'HH:MM:SS'), '->' , datestr(datevec(now) + [0,0,0,0,0,validExpTime], 'HH:MM:SS')])
        end
        t = tic;
        image = self.AcquireData();
        
%         keyboard
        
        time = toc(t);
%         if self.verbose

%         end
        
        if all(image(:)==0)
            warning('All acquired values are zero ')
            keyboard
        end
        
        
%         data_all(:,:,i) = (reshape(data,self.XPixels/self.Binning/self.Cropping, self.YPixels/self.Binning/self.Cropping));
        image = reshape(image, Npix);
                
%         keyboard
%         oversat_tot = oversat_tot | image <= 0 ;
        if self.getExposure ~= self.exposure_range(1) && any(image(:) > self.sat_level)
             oversat =  image > self.sat_level*0.8;
        else
            %% weaker condition for the shortest exposure possible 
             oversat = image > self.sat_level*0.95;
        end
        oversat = oversat  | image <= 0 ;
        oversat_tot = oversat_tot | oversat;
        
       if mean(oversat(:)) > 1e-4 && (self.getExposure ~= self.exposure_range(1) )
           oversat = imdilate(oversat,strel('line', 5, 0));
       end

%         if mean(oversat(:)) > 0.8
%             continue
%         end
    
        
        fprintf('Exposure = %3.3g s   Readout = %3.3g Temp=%i Exp.Temp=%i oversat=%g \n', ...
            validExpTime,  time - validExpTime, self.getTemp,self.Temp, mean2(oversat))


% disp('replace oversat ')
%             image  = replace_nans(image, oversat);
        try
            data(:,:,i)= image; 
        catch
            keyboard
        end
    
    end
catch
    keyboard
end

    
%     keyboard
    
    if ~r.fast_mode
        self.ShutdownCamera()
    end
    if nargout ~= 0
        data_out = data;
    end
    

%     
% end
    

% keyboard

    current_time = now;
    if r.auto_save
        object = self.Save(data, ...
            ['AndorData_', datestr(current_time, 'HH-MM-SS')], ...
            'ReadOutTime', time - validExpTime ); 
    end
    if ~isempty(r.save_path)
        self.Save(data, ...
        ['AndorData_', datestr(current_time, 'HH-MM-SS')], ...
        'base_path', r.save_path, ...
        'ReadOutTime', time - validExpTime ); 
    end
    if r.Show ||  nargout == 0
        fig = figure(786482);
        self.Show('data', data, 'curr_fig', fig , 'logscale', r.logscale);
    end
end

