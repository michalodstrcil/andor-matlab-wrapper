function data_out = Show(self, varargin )
    %% Author: Michal Odstrcil, mo1e13@soton.ac.uk, University Southampton

    par = inputParser;
    par.KeepUnmatched = true;
    par.addOptional('data', [])
    par.addParameter('get_background',  false , @islogical )
    par.addParameter('logscale', false, @islogical)
    par.addParameter('visible', 'on', @isstr)
    par.addParameter('curr_fig', [])
    par.addParameter('enhance_image', true, @islogical )
    par.addParameter('id', randi(10000), @isint )

    par.parse(varargin{:})
    r = par.Results;
    data = r.data;
    
    xgrid = 1:size(data,1); ygrid = 1:size(data,2);
    xpos = [];ypos = [];

%     keyboard
    
    if size(data,2) == 3  % 3 columns
        N = 100;
        xpos = data(:,1);
        ypos = data(:,2);
        values = data(:,3);
        xgrid = linspace( min(xpos), max(xpos), N);
        ygrid = linspace( min(ypos), max(ypos), N);
        [X,Y] = meshgrid(xgrid, ygrid);
        data = griddata(xpos,ypos,values,  X, Y);
%         data(data(:)<0) = 0;
        r.enhance_image = false;
    elseif isempty(data)
        [data,~, self] = self.GetData(varargin{:});
    end
      


    if isobject(data)
        data = data.Data;
    end
        
    data = mean( single(data),3);
    
    if ~isvector(data) && r.enhance_image
        % remove hot pixels 
        fdata = data - medfilt2(data, [3,3], 'symmetric');
%         keyboard
        data( abs(fdata) > 10*mad(fdata(fdata>0),1)) = nan;
        data(data > quantile(data(:), 0.9999) | data < quantile(data(:), 0.0001)) = nan;
        data = replace_nans(data);
        data = replace_nans(data);
    end
    
    
    if r.get_background
          data = data - self.GetBackground();
          data(data < 0) = 0 ; 
    end
    
    
    
    if r.logscale
         posit = @(x)((abs(x)+x)/2);
%          data(data < 0) = nan;
         data = data -  nanmin(data(:));
         data = quantile(data(data > 0), 0.01) + posit(data);
    end
    
    if isempty(r.curr_fig)
         figure(r.id)
         set(gcf, 'visible',r.visible, 'PaperPositionMode','auto');
    end
    
     if isvector(data)
        plot(data)
     else
         
%          keyboard
        load MyColormap
        if r.logscale
            imagesc_log(2+(data));
        else
            imagesc(xgrid, ygrid, data)
            colorbar()
        end
        colormap(HDR_cmap(1:8:end, :))
        if ~isempty(xpos)
            hold on
            plot(xpos, ypos, 'k.')
            hold off
        end
     end
    if nargout == 1
        data_out = data;
    end
end