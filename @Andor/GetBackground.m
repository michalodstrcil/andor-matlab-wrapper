function [DarkNoise_out,ReadOutNoise, DeadPixelMap, ShotNoise] = ...
    GetBackground(self, varargin)
    %% Author: Michal Odstrcil, mo1e13@soton.ac.uk, University Southampton


    par = inputParser;
    par.KeepUnmatched = true;
    par.addParameter('Exposure',nan, @isnumeric )
    par.addParameter('NumAcc', 1, @isint )
%     par.addParameter('Temp', self.Temp, @isnumeric )
    par.addParameter('ReadOutTimeInd', self.ReadOutTimeInd, @isnumeric )
    par.addParameter('Binning', 1, @isint )
    par.addParameter('Cropping', 1, @isint )
    par.addParameter('Show', false, @islogical )
    par.addParameter('fast_mode', true, @islogical )
    par.addParameter('crop_center', [] , @isnumeric)

    par.parse(varargin{:})
    r = par.Results;

    disp('Cleaning camera')
    self.Clear();
    disp('Reading background')
     
    
    varargin = [varargin , 'fast_mode', true];

    LongExp = nanmax(r.Exposure, 5*max(1,self.ReadOutTimeInd)^2*sqrt(r.NumAcc));  % estimation of the readout time 
    ShortExp = 0.1;
%     if isnan(r.Exposure)
%         ExpTime = self.getExposure;
%     else
%         ExpTime = r.Exposure;
%     end
%     if isempty(r.crop_center)
%        r.crop_center =  self.getAllPix()/2;
%     end
    
%     keyboard
    
    self.setBinning(r.Binning);
%     Npix = self.getNpix();
%     [~, ROI]=crop_image(true(Npix), r.Cropping, r.crop_center);

% keyboard

    if ~isempty(r.crop_center) && r.Cropping == 1
        warning('Cropping is 1')
        r.crop_center = [];
    end
    
    Np_all = self.getAllPix() / self.Binning;
    [~, ROI]=crop_image(true(Np_all), r.Cropping, r.crop_center());
%         keyboard
%     X0 = [ROI{1}(1),ROI{2}(1)]-1;
%     dX = [length(ROI{1}),length(ROI{2})];
    
    args = {'ROI',ROI,'save_path', '', 'Show', false, 'auto_save', false, 'ShutterMode', 2, 'GetBackground',false};

     Noise = [];
%     if nargout == 1  %  Fast mode if I want only noise !!! 
%         Noise = single(self.GetData(varargin{:}, args{:}, 'Exposure', ExpTime));
%         Noise = mean(Noise, 3);
%         return
%     end
    
    % clean ccd ?? 
%     for i = 1:5
%     data{1} = GetData(ShortExp,NumAcc,ReadOutTime, Temp, ShutterMode, fast_mode );
%     imagesc(data{1}0); colorbar
%     drawnow
%     end

    data{1} = single(self.GetData( varargin{:}, args{:},'Exposure', ShortExp, 'Show', false, 'NumAcc', 3*r.NumAcc));                          
    data{2} = single(self.GetData( varargin{:}, args{:},'Exposure', LongExp, 'Show', false));
          
%     if nargout > 1
%         if ~isnan(r.Exposure)
%             data{3} = single(self.GetData(  varargin{:}, args{:}, 'Exposure', r.Exposure, 'Show', false));
%             Noise = mean(data{3}, 3);
%         else 
%             Noise = [];
%         end
%     end
    
    
%     keyboard
    
    Npix = self.getNpix;
    filt_ker = 5;
    
    DeadPixelMap = false(Npix);
    for i = 1:length(data)
         pattern_filt{i} = medfiltN(data{i}, [filt_ker,filt_ker]);
         pattern_diff{i} = data{i} - pattern_filt{i}  ;
         if i > 1   % not for the readout noise !! 
             thresh = median(pattern_diff{i}(:))+10*mad(pattern_diff{i}(:), 1);
             DeadPixelMap =  DeadPixelMap | any( pattern_diff{i} > thresh, 3);
         end
%          pattern_mean{i} = median(data{i},3);
    end
    fprintf('N hot pixels about %i \n ',sum(DeadPixelMap(:)))
    
    %% !!  aproximation !! of the expected noise in image after background subtraction
    ShotNoise = std(data{1}, [],3) +std(data{2}, [],3)  ;
    ShotNoise = medfilt2(ShotNoise, [3,3], 'symmetric');
    ShotNoise = denoise(ShotNoise, mean(Npix)/80, 'gauss2', true);
    
        

    
%     keyboard
    

%     keyboard
    
    %% get median value instead of averaging in case of multiaccum images 
    for i = 1:2
        if size(data{i},3) > 2
            data{i} = sort(data{i},3);
            data{i} = single(data{i}(:,:,max(1,round(end/2))));
        elseif size(data{i},3) == 2
            data{i} = mean(data{i},3);
        end
    end
    
    
    
%     for i = 1:2
%         data{i}=  trimmed_mean(data{i},3, 0.4);
%     end
        
    DarkNoise = (data{2} - data{1}) / ( LongExp -  ShortExp) ;
    ReadOutNoise = data{1} - DarkNoise * ShortExp;
    DarkNoise = posit(DarkNoise);
    ReadOutNoise = posit(ReadOutNoise);
%     DarkNoise = mean(DarkNoise,3);
%     ReadOutNoise = mean(ReadOutNoise,3);

% keyboard

    fprintf('Dark noise %g  Readout noise m=%g s=%g \n ', mean(DarkNoise(:)), mean(ReadOutNoise(:)), std(ReadOutNoise(:)) )
    
    ReadOutNoise = replace_nans(ReadOutNoise, DeadPixelMap);
    DarkNoise = replace_nans(DarkNoise, DeadPixelMap);
    if r.NumAcc < 2
        DarkNoise = medfilt2(DarkNoise, [3,3], 'symmetric');
    end
    DarkNoise = denoise(DarkNoise, 1/r.Binning/r.NumAcc, 'gauss2', true);

    
%     keyboard
    
    if exist('Noise') && ~isempty(Noise)
        Noise(DeadPixelMap) =  nan;
        Noise = replace_nans(Noise, DeadPixelMap);
    else
        Noise = ReadOutNoise + DarkNoise * self.getExposure;
    end
    
    if r.Show || nargout == 0
        figure
        subplot(1,3,1)
        % DarkNoise_clean = medfilt2(DarkNoise, [5,5], 'symmetric');
        imagesc(DarkNoise); axis off image ; colorbar; title('Dark noise per second')
        subplot(1,3,2)
        imagesc(ReadOutNoise, [quantile(ReadOutNoise(:), 0.01), ...
            quantile(ReadOutNoise(:), 0.99)]); axis off image ; colorbar; title('Readout noise')
        subplot(1,3,3)
        imagesc(ShotNoise, [quantile(ShotNoise(:), 0.01), ...
            quantile(ShotNoise(:), 0.99)]); axis off image ; colorbar; title('ShotNoise noise')
  
    end
    
%     keyboard
    if nargout > 1
        DarkNoise_out  = DarkNoise;
    elseif nargout == 1
        DarkNoise_out = {DarkNoise,ReadOutNoise, DeadPixelMap, ShotNoise};
    end


    if ~r.fast_mode
        self.ShutdownCamera()
   end
end


