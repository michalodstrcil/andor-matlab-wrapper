function object_out = Save(self, Data, name, varargin )
        %% Author: Michal Odstrcil, mo1e13@soton.ac.uk, University Southampton
        % example of use   andor.Save(date, 'my data', 'base_path', pwd )
        if nargin == 1
            Data = self.GetData(varargin{:});
        end
        if nargin == 2
            name = ['AndorData_', datestr(now, 'HH-MM-SS')];
        end
        par = inputParser();
        par.KeepUnmatched = true;
        par.addParameter('base_path', 'C:\AndorData' , @isstr)
        par.addParameter('Noise', [], @isnumeric)
        par.addParameter('Mask', [], @islogical)
        par.addParameter('inputs', {}, @iscell)
        par.addParameter('Background', {})
        par.addParameter('Comment', '', @isstr)
        par.addParameter('logscale', false, @islogical )

        par.parse(varargin{:})
        r = par.Results;

        if isstruct(Data)
            Data = Data.Data;
        end
        path = [r.base_path,'\', datestr(now, 'yyyymmdd'), '\'];
        if ~exist(path, 'file')
            mkdir(path);
        end
        ExposureTime = self.Exposure;
        xps_pos = [];
        try
             xps_pos = self.XPS.get_pos();
        end
        smaract_pos = [];
        try
            smaract_pos = self.Smaract.get_pos();
        end
        [~,Temp] = GetTemperature();
        
        Pressure = nan;
        ReadOutTime = self.ReadOutTime;
        Noise = r.Noise;
        Mask = r.Mask;
        Binning = self.Binning;
        Gain = self.getGain;
        inputs = r.inputs;
        Background = r.Background;
        Comment = r.Comment;
        Cropping = self.Cropping;

        time = datestr(now);
        % save original data 
        save([path, name, '.mat'], 'Data','Noise','Mask', ...
            'ExposureTime', 'Temp', ...
            'Pressure','ReadOutTime', 'Binning','Cropping', 'Gain', 'time',  ...
            'xps_pos', 'smaract_pos', 'inputs', 'Background', 'Comment');
                
        
        % create the image 
        self.Show('data', Data, 'visible', 'off','logscale', r.logscale);
        % save image 
        title(sprintf('Exp=%3.1g ReadOut=%i Binning=%i Temp=%i Gain=%i Max=%g',...
            self.Exposure, self.ReadOutTimeInd, self.Binning, self.getTemp, self.getGain, max(Data(:))))
        colorbar
        disp([ 'Saving data to: ', path, name])
        print('-dpng', [path, name, '.png'])
                
        %%  add some metainformations to the image !!!!!!!!!!! 
        image = imread([path, name, '.png']);  
%          image_info = imfinfo([path, name, '.png']);
        % unfortunatelly it is not easily readable for PNG 
        imwrite(image, [path, name, '.png'], 'Comment',...
            [['Comment:', r.Comment] ,[' ExposureTime=', num2str(ExposureTime)], ...
            [' Temp=', num2str(ExposureTime)], [' Pressure=', num2str(Pressure)], ...
           [ ' ReadOutTime=', num2str(ReadOutTime)], [' Binning=', num2str(Binning)], ...
            [' Cropping=', num2str(Cropping)], [' time: ', datestr(now, 'yyyy-mm-dd'), ' ', ...
            datestr(now, 'HH:MM:SS') ]]);

        if nargout == 1
           object_out = load([path, name, '.mat']);
        end
%         close()
        

end
