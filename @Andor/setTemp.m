function setTemp(self,newTemp, fast_mode)
    %% Author: Michal Odstrcil, mo1e13@soton.ac.uk, University Southampton
    if nargin < 3
        fast_mode =  true;
    end
    
    self.Temp = newTemp;

    if isnan(newTemp); return ; end
    self.PrepareCamera;

    self.Abort();  % stop previous acquisitions 

    newTemp = round(newTemp);

    if newTemp < 20
        [~]=CoolerON;
    else
        [~]=CoolerOFF;
    end


    if fast_mode && self.getTemp - newTemp < 5
      [~]=SetTemperature(newTemp);
        return
    end

    
    [~]=SetTemperature(newTemp-2);
    i = 1;
%     figure(12313)
%     hold on


    while  self.getTemp > newTemp 
        pause(1.0);
        fprintf('Cooling Time %is Temp %i  Requested %i \n  ', i,   self.getTemp, newTemp)
        i = i+1;
    end
    [~]=SetTemperature(newTemp-1);




%     hold off
   
end