function [data_out, self] = GetFVB(self, varargin )
    %% Author: Michal Odstrcil, mo1e13@soton.ac.uk, University Southampton


% keyboard

    par = inputParser;
    par.KeepUnmatched = true;
    par.addParameter('Exposure',  0, @isnumeric)
    par.addParameter('Temp', self.Temp, @isnumeric)
%     par.addParameter('ShutterMode', 0, @isnumeric)
    par.addParameter('ReadOutTime', 1, @isnumeric)
    par.addParameter('Show', false, @islogical)
    par.addParameter('auto_save', false, @islogical)

    par.parse(varargin{:})
    r = par.Results;


% keyboard
    self = self.PrepareCamera;
    
    self.setROI(0,0,1024, 1024)


    %%%%%%%%%%%%%%%%%%%%%%% COOLING %%%%%%%%%%%%%%%%%%%%
    self.setTemp(r.Temp)
    %%%%%%%%%%%%%%%%%%%%%%% COOLING %%%%%%%%%%%%%%%%%%%%
    [~,currentTemp]=GetTemperature;
    if currentTemp >= self.MaxTemperature
        self.ShutdownCamera()
        currentTemp
        error('Camera temperature is too high, shutting down camera ...  (adjust MaxTemperature limit)')
    end
    
    
%     self=self.setReadOutSpeed(r.ReadOutTime);

%     self=self.setGain(self.gain);
%     self=self.setBinning(1);  % it seems that it is not possible to bin it more

%     keyboard
    
% return 

    if ~self.FVB || self.Exposure ~= r.Exposure     
        disp('InitFVB')
        self=self.setShutter(1);  % always opened
        self=self.setExposure(r.Exposure);
        self = self.InitFVB();
    end

    
    [~,validExpTime,~,~]=GetAcquisitionTimings;

    t = tic;
    data = single(self.AcquireData());
    [~,currentTemp]=GetTemperature;
%     data( abs(data - median(data)) > 6*mad(data,1)) = nan;

    time = toc(t);
    fprintf('Exposure = %g   Readout = %g Temp %g \n ',...
        validExpTime,  time - validExpTime, currentTemp)

    if r.auto_save
        disp('autosave')
        object = self.Save(data, ...
            ['AndorFVBData_', datestr(now, 'HH-MM-SS-FFF')], ...
            'base_path', 'C:\AndorData', ...
            'ReadOutTime', time - validExpTime ); 
    end
    
     if r.Show ||  nargout == 0
         try
            figure
            plot(data)
            xlim([1,length(data)])
            drawnow
         catch
            keyboard
         end
         
     end
    if nargout > 0
        data_out = data;
    end
    

end


